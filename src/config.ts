import changementCard from "./assets/img1.png";
import sensCard from "./assets/img2.png";
import valeursCard from "./assets/img3.png";
import moyensCard from "./assets/img4.png";
import reconnaissanceCard from "./assets/img5.png";
import organisationCard from "./assets/img6.png";
import expositionCard from "./assets/img7.png";
import sensCadreCard from "./assets/SENS.Cadre.png";
import sensCapCard from "./assets/SENS.Cap.png";
import sensEquipeCard from "./assets/SENS.Equipe.png";
import sensEtapeCard from "./assets/SENS.Etape.png";

import changeCycleCard from "./assets/CHG.Cycle.png";
import changeFluxCard from "./assets/CHG.Flux.png";
import changeInteretsCard from "./assets/CHG.Interets.png";
import changeSaisonaliteCard from "./assets/CHG.Saisonalite.png";
import DiagnostiqueCard from "./assets/Diagnostique.png";

export const allCards = [
    {
        id: 1,
        image: changementCard,
        y: 130
    },
    {
        id: 2,
        image: sensCard,
        y: 130
    },
    {
        id: 3,
        image: valeursCard,
        y: 130
    },
    {
        id: 4,
        image: moyensCard,
        y: 130
    },
    {
        id: 5,
        image: reconnaissanceCard,
        y: 130
    },
    {
        id: 6,
        image: organisationCard,
        y: 130
    },
    {
        id: 7,
        image: expositionCard,
        y: 130
    },
]

export const allMozaicCards = [
    {
        id: 1,
        image: sensCadreCard,
        y: 130,
        url: "https://jfallet.wordpress.com/2023/07/03/mon-job-et-moi-comment-ca-va/"
    },
    {
        id: 2,
        image: sensCapCard,
        y: 130,
        url: "https://jfallet.wordpress.com/2023/07/03/mon-job-et-moi-comment-ca-va/"
    },
    {
        id: 3,
        image: sensEquipeCard,
        y: 130,
        url: "https://jfallet.wordpress.com/2023/07/03/mon-job-et-moi-comment-ca-va/"
    },
    {
        id: 4,
        image: sensEtapeCard,
        y: 130,
        url: "https://jfallet.wordpress.com/2023/07/03/mon-job-et-moi-comment-ca-va/"
    },
    {
        id: 5,
        image: changeCycleCard,
        y: 130,
        url: "https://jfallet.wordpress.com/2023/07/03/mon-job-et-moi-comment-ca-va/"
    },
    {
        id: 6,
        image: changeFluxCard,
        y: 130,
        url: "https://jfallet.wordpress.com/2023/07/03/mon-job-et-moi-comment-ca-va/"
    },
    {
        id: 7,
        image: changeInteretsCard,
        y: 130,
        url: "https://jfallet.wordpress.com/2023/07/03/mon-job-et-moi-comment-ca-va/"
    },
    {
        id: 8,
        image: changeSaisonaliteCard,
        y: 130,
        url: "https://jfallet.wordpress.com/2023/07/03/mon-job-et-moi-comment-ca-va/"
    },
    {
        id: 9,
        image: DiagnostiqueCard,
        y: 130,
        url: "https://jfallet.wordpress.com/2023/07/03/mon-job-et-moi-comment-ca-va/"
    },
]
