import {allMozaicCards} from "./config.ts";
import xlg from "./assets/x-lg.svg";
import './Mosaic.css'

type Props = {
    close: () => void
}

const Mosaic = ({close}: Props) => {
    return (
        <>
            <img src={xlg} className={"mosaic-button"} title={"Fermer"}
                 onClick={() => close()}/>
            <div className="mosaic">
                {allMozaicCards.map((movingMotivatorsCard) => (
                    <a href={movingMotivatorsCard.url} target="_blank"><img src={movingMotivatorsCard.image}/></a>
                ))}
            </div>
        </>
    )
}

export default Mosaic