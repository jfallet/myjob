import './Instructions.css'

type InstructionsProps = {
    step: number
    updateStep: (step: number) => void
}

const Instructions = ({step, updateStep}: InstructionsProps) => {

    const actionButton = () => {
        if (step === 1) {
            return <button onClick={() => updateStep(2)}>Evaluer</button>
        } else {
            return <button onClick={() => updateStep(1)}>Classer</button>
        }
    }

    const instructions = () => {
        if (step === 1) {
            return (
                <>
                    <span style={{fontWeight: "bold"}}>Classer les besoins  {actionButton()}</span>
                    <p>Prenez le temps de lire les cartes.
                    Déplacez le plus à droite celles qui semblent le plus vous impacter.
                    <i>(Obtenez plus de détail sur l'atelier dans cet <a href="https://jfallet.wordpress.com/2023/07/03/mon-job-et-moi-comment-ca-va/" target="_blank">article</a>)</i></p>
                </>
            )
        } else {
            return (
                <>
                    <span style={{fontWeight: "bold"}}>Evaluer les besoins  {actionButton()}</span>
                    <p>Déplacer vers le haut les besoins qui sont correctement nourris.
                    Déplacez vers le bas les besoins qui ne le sont pas.
                    <i>(Obtenez plus de détail sur l'atelier dans cet <a href="https://jfallet.wordpress.com/2023/07/03/mon-job-et-moi-comment-ca-va/" target="_blank">article</a>)</i></p>
               </>
            )
        }
    }

    return (
        <>
            <div className={"instructions"}>
                {instructions()}
            </div>
        </>
    )
}

export default Instructions;