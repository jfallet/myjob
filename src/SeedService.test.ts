import {expect, test} from 'vitest'
import {seedExtracter, seedForger} from './SeedService.ts'
import changementCard from "./assets/img1.png";
import sensCard from "./assets/img2.png";
import valeursCard from "./assets/img3.png";
import moyensCard from "./assets/img4.png";
import reconnaissanceCard from "./assets/img5.png";
import organisationCard from "./assets/img6.png";
import expositionCard from "./assets/img7.png";

test('should forge seeds', () => {
    const cards =
        [
            {
                id: 7,
                image: expositionCard,
                y: 60
            },
            {
                id: 6,
                image: organisationCard,
                y: 50
            },
            {
                id: 5,
                image: reconnaissanceCard,
                y: 40
            },
            {
                id: 4,
                image: moyensCard,
                y: 30
            },
            {
                id: 3,
                image: valeursCard,
                y: -20
            },
            {
                id: 2,
                image: sensCard,
                y: 10
            },
            {
                id: 1,
                image: changementCard,
                y: 0
            },
        ]
    const forgedSeed = seedForger(cards)
    expect(forgedSeed).toStrictEqual("7!60|6!50|5!40|4!30|3!-20|2!10|1!0")
})

test('should extract cards', () => {
    const extractedCards = seedExtracter("7!60|6!50|5!40|4!30|3!-20|2!10|1!0")
    expect(extractedCards).toStrictEqual([
        {
            id: 7,
            image: expositionCard,
            y: 60
        },
        {
            id: 6,
            image: organisationCard,
            y: 50
        },
        {
            id: 5,
            image: reconnaissanceCard,
            y: 40
        },
        {
            id: 4,
            image: moyensCard,
            y: 30
        },
        {
            id: 3,
            image: valeursCard,
            y: -20
        },
        {
            id: 2,
            image: sensCard,
            y: 10
        },
        {
            id: 1,
            image: changementCard,
            y: 0
        },
    ])
})